function find(items,callback){
    if(items === undefined || items.length === 0) return [];

    let elem;
    for(let i = 0; i < items.length; i++){
        if(callback(items[i])){
            elem = items[i];
            return elem;
        }
    }
    return elem 
}
module.exports = find;