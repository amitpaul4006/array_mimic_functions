function reduce(element,callback,start_val){
    let copy_element = [...element]
    if(element === undefined || element.length === 0) return [];
    
    if(start_val === undefined){
        start_val = element[0];
        copy_element = copy_element.slice(1);
    }
    
    let val = start_val
    for(let index=0; index<copy_element.length; index++){
        val = callback(val, copy_element[index], index)
    }
    return val

}

module.exports = reduce;
