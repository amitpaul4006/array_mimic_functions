function map(elements,callback) {
    if(elements === undefined || elements.length === 0) return [];
    
    let new_array = []; 
    for(let i = 0; i < elements.length; i++){
        if(elements[i]){
            new_array.push(callback(elements[i]));
        }
    }
    return new_array;
}
module.exports = map;