const flatten = (arr) => {
    let final_arr = []
    
    for (let i = 0; i < arr.length; i++) {
        
        if (typeof (arr[i]) == 'object') {
            final_arr.push(...flatten(arr[i]))
        } else {
            (final_arr.push(arr[i]))
            }
        }
        return final_arr
}

module.exports = flatten;

