function each(element,callback) {
    if(element === undefined || element.length === 0) return [];

    for(let i = 0; i < element.length; i++){
        callback(i,element[i]);
    }
}

module.exports = each;