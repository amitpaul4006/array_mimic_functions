function filter(items,callback){
    if(items === undefined || items.length === 0) return [];

    let arr_elem = []
    for(let i = 0; i < items.length; i++){
        if(callback(items[i])){
            arr_elem.push(items[i])
        }
    }
    return arr_elem
}
module.exports = filter;