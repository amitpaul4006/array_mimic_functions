let { items } = require('../data');
let for_each = require('../each');

function callback(index,element) {
    console.log(`This is index ${index} and This is element ${element}`);
}

const result = for_each(items,callback);
if(result){
    console.log(result);
}